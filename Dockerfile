# syntax=docker/dockerfile:1

FROM python:3.10-alpine3.16

WORKDIR /src/

COPY ./requirements.txt ./requirements.txt

RUN pip install --no-cache-dir --upgrade -r ./requirements.txt

COPY ./src/*.py ./

ENTRYPOINT ["python", "exporter.py"]
