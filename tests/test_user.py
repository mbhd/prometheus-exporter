import pytest

from src.user_exporter import UserCollector


def test_user_collect(mocker, users_data_fixture):
    # GIVEN a mocked request which returns a fixture with test data
    mocker.patch('requests.get', return_value=users_data_fixture)
    # GIVEN an instance of a class that has a collect method
    collector = UserCollector()

    # WHEN iterating over collector.collect
    generator = collector.collect()
    sample = next(generator)
    users = sample.samples[0].value

    # THEN assert actual equals expected
    assert users == 4


@pytest.mark.onedevskip
def test_user_exception(users_data_fixture):
    """Test without mocking request.get
    This will throw a ValueError when trying to unpack tuple
    because the tuple will not contain any values"""
    with pytest.raises(StopIteration):
        collector = UserCollector()
        generator = collector.collect()
        sample = next(generator).samples[0].value
