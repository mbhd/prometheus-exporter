import pytest

from src.projects import ProjectsCollector


def test_projects_collect(mocker, projects_data_fixture):
    # GIVEN a mocked request which returns a fixture with test data
    mocker.patch('requests.get', return_value=projects_data_fixture)
    # GIVEN an instance of a class that has a collect method
    collector = ProjectsCollector()

    # WHEN iterating over collector.collect
    generator = collector.collect()
    sample = next(generator)
    projects = sample.samples[0].value

    # THEN assert actual equals expected
    assert projects == 3


@pytest.mark.onedevskip
def test_projects_exception(projects_data_fixture):
    """Test without mocking request.get
    This will throw a ValueError when trying to unpack tuple
    because the tuple will not contain any values"""
    with pytest.raises(StopIteration):
        collector = ProjectsCollector()
        generator = collector.collect()
        sample = next(generator).samples[0].value
