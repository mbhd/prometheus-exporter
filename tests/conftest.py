import pytest
import json
from pathlib import Path


class Response:
	def __init__(self, json_data):
		self.json_data = json_data

	def json(self):
		return self.json_data


@pytest.fixture
def pull_req_data() -> Response:
	path = Path.cwd() / 'tests/fixtures/pull_requests_data.json'
	with open(path, 'r') as file:
		return Response(json_data=json.load(file))


@pytest.fixture
def projects_data_fixture() -> Response:
	with open('tests/fixtures/projects_data.json', 'r') as file:
		return Response(json_data=json.load(file))


@pytest.fixture
def build_data_fixture() -> Response:
	with open('tests/fixtures/build_data.json', 'r') as file:
		return Response(json_data=json.load(file))


@pytest.fixture
def users_data_fixture() -> Response:
	with open('tests/fixtures/users_data.json', 'r') as file:
		return Response(json_data=json.load(file))
