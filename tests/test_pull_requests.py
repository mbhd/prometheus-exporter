import pytest
from src.pull_requests import PullRequestsCollector


def test_result(mocker, pull_req_data):
	
	mocker.patch('requests.get', return_value=pull_req_data)

	# takes OPEN, MERGED, DISCARDED
	collector = PullRequestsCollector()
	pull_req_func = collector.collect()
	status_open, status_merged, status_discarded = (i.samples[0].value for i in pull_req_func)

	assert status_open == 1
	assert status_merged == 2
	assert status_discarded == 1


@pytest.mark.onedevskip
def test_result_exception(pull_req_data):
	"""Test without mocking request.get
	This will throw a ValueError when trying to unpack tuple
	because the tuple will not contain any values"""
	with pytest.raises(ValueError):
		collector = PullRequestsCollector()
		pull_req_func = collector.collect()
		status_open, status_merged, status_discarded = (i.samples[0].value for i in pull_req_func)

