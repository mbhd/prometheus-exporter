import pytest

from src.build_collector import BuildsCollector


def test_build_collector(mocker, build_data_fixture):
    # GIVEN a mocked request which returns a fixture with test data
    mocker.patch('requests.get', return_value=build_data_fixture)
    # GIVEN an instance of a class that has a collect method
    collector = BuildsCollector()

    # WHEN iterating over collector.collect
    desc_func = collector.collect()
    successful, failed, waiting = (i.samples[0].value for i in desc_func)

    # THEN assert actual equals expected
    assert successful == 1
    assert failed == 1
    assert waiting == 1


@pytest.mark.onedevskip
def test_build_collector_exception(build_data_fixture):
    """Test without mocking request.get
    This will throw a ValueError when trying to unpack tuple
    because the tuple will not contain any values"""
    with pytest.raises(ValueError):
        # GIVEN an instance of a class that has a collect method
        collector = BuildsCollector()

        # WHEN calling collector.collect
        desc_func = collector.collect()
        x, y, z = (i.samples[0].value for i in desc_func)


