"""custom exporter"""

from os import environ

from prometheus_client import start_http_server, Metric, REGISTRY

import requests
from requests.auth import HTTPBasicAuth

ONEDEV_USER = environ['ONEDEV_USER']
ONEDEV_PASSWORD = environ['ONEDEV_PASSWORD']
IP_ADDRESS = environ['IP_ADDRESS']


class UserCollector:
    """Exporter for OneDev endpoint /users"""

    @staticmethod
    def collect():
        """Collect data from OneDev API
        Data collected is number of users in OneDev.
        """

        try:
            response_users = requests.get(f'http://{IP_ADDRESS}:6610/api/users?offset=0&count=100',
                                          auth=HTTPBasicAuth(ONEDEV_USER, ONEDEV_PASSWORD),
                                          timeout=15)

            metric_u = Metric('onedev_number_of_users', 'Number of users', 'gauge')

            count_users = 0
            for _ in response_users.json():
                count_users += 1

            metric_u.add_sample('onedev_number_of_users', value=count_users, labels={})
            yield metric_u

        except requests.exceptions.RequestException as err:
            print(f'\nSomething happened:\n\n{err}\n')


if __name__ == '__main__':  # pragma: no cover
    start_http_server(8000)
    REGISTRY.register(UserCollector())
    while True:
        pass
