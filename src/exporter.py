"""Custom exporter for OneDev metrics"""

from prometheus_client import start_http_server, REGISTRY

from pull_requests import PullRequestsCollector
from build_collector import BuildsCollector
from projects import ProjectsCollector
from user_exporter import UserCollector


if __name__ == '__main__':  # pragma: no cover
	start_http_server(8000)
	REGISTRY.register(PullRequestsCollector())
	REGISTRY.register(BuildsCollector())
	REGISTRY.register(ProjectsCollector())
	REGISTRY.register(UserCollector())
	# -- Register all imported exporter classes here --

	# --											 --
	while True:
		pass
