"""Custom Prometheus exporter"""

from os import environ

from prometheus_client import start_http_server, Metric, REGISTRY

import requests
from requests.auth import HTTPBasicAuth

ONEDEV_USER = environ['ONEDEV_USER']
ONEDEV_PASSWORD = environ['ONEDEV_PASSWORD']
IP_ADDRESS = environ['IP_ADDRESS']


class PullRequestsCollector:
    """Exporter for OneDev endpoint /pull-requests"""

    @staticmethod
    def collect():
        """Collect data from OneDev API
        Data collected is the status for all Pull Requests in OneDev.
        The states we are collecting:
        OPEN: If the pull request is open
        MERGED: If the pull request is merged
        DISCARDED: If the pull request is discarded
        """

        try:
            resp = requests.get(f'http://{IP_ADDRESS}:6610/api/pull-requests?offset=0&count=100',
                                auth=HTTPBasicAuth(ONEDEV_USER, ONEDEV_PASSWORD),
                                timeout=15)

            metric_open = Metric('onedev_open_pull_requests', 'Open Pull Requests', 'gauge')
            metric_merged = Metric('onedev_merged_pull_requests', 'Merged Pull Requests', 'counter')
            metric_discarded = Metric('onedev_discarded_pull_requests', 'Discarded Pull Requests', 'counter')

            open_req = 0
            merged_req = 0
            discarded_req = 0
            for pull_request in resp.json():
                if pull_request['status'] == 'OPEN':
                    open_req += 1
                elif pull_request['status'] == 'MERGED':
                    merged_req += 1
                elif pull_request['status'] == 'DISCARDED':
                    discarded_req += 1

            metric_open.add_sample('onedev_open_pull_requests', value=open_req, labels={})
            metric_merged.add_sample('onedev_merged_pull_requests', value=merged_req, labels={})
            metric_discarded.add_sample('onedev_discarded_pull_requests', value=discarded_req, labels={})

            yield metric_open
            yield metric_merged
            yield metric_discarded

        except requests.exceptions.RequestException as err:
            print(f'\nSomething happened:\n\n{err}\n')


if __name__ == '__main__':  # pragma: no cover
    start_http_server(8000)
    REGISTRY.register(PullRequestsCollector())
    while True:
        pass
