"""Custom Prometheus exporter"""

from os import environ

from prometheus_client import start_http_server, Metric, REGISTRY

import requests
from requests.auth import HTTPBasicAuth

ONEDEV_USER = environ['ONEDEV_USER']
ONEDEV_PASSWORD = environ['ONEDEV_PASSWORD']
IP_ADDRESS = environ['IP_ADDRESS']


class ProjectsCollector:
    """Exporter for OneDev endpoint /projects"""

    @staticmethod
    def collect():
        """Collect data from OneDev API
        Data collected is the number of projects in OneDev
        """

        try:
            response_projects = requests.get(f'http://{IP_ADDRESS}:6610/api/projects?offset=0&count=100',
                                             auth=HTTPBasicAuth(ONEDEV_USER, ONEDEV_PASSWORD),
                                             timeout=15)

            metric_projects = Metric('onedev_projects', 'Number of Projects in OneDev', 'gauge')

            count_projects = 0
            for _ in response_projects.json():
                count_projects += 1

            metric_projects.add_sample('onedev_projects', value=count_projects, labels={})
            yield metric_projects

        except requests.exceptions.RequestException as err:
            print(f'\nSomething happened:\n\n{err}\n')


if __name__ == '__main__':  # pragma: no cover
    start_http_server(8000)
    REGISTRY.register(ProjectsCollector())
    while True:
        pass
