"""custom exporter for OneDev"""

from os import environ

from prometheus_client import start_http_server, Metric, REGISTRY

import requests
from requests.auth import HTTPBasicAuth

ONEDEV_USER = environ['ONEDEV_USER']
ONEDEV_PASSWORD = environ['ONEDEV_PASSWORD']
IP_ADDRESS = environ['IP_ADDRESS']


class BuildsCollector:
    """Exporter for OneDev endpoint /build"""

    @staticmethod
    def collect():
        """Collect data from OneDev API
        Data collected is the status for all build jobs.
        The states we are collecting:
        SUCCESSFUL: If the build was successful
        FAILED: If the build failed
        WAITING: If the build job is waiting

        For more complete metrics we could also implement the following states:
        PENDING, RUNNING, CANCELLED, TIMED_OUT
        """
        try:
            response_builds: requests.Response = requests.get(f'http://{IP_ADDRESS}:6610/api/builds?offset=0&count=100',
                                                              auth=HTTPBasicAuth(ONEDEV_USER, ONEDEV_PASSWORD),
                                                              timeout=15)

            metric_w: Metric = Metric('onedev_waiting_builds', 'Waiting builds', 'gauge')
            metric_s: Metric = Metric('onedev_successful_builds', 'Successful builds', 'counter')
            metric_f: Metric = Metric('onedev_failed_builds', 'Failed builds', 'counter')

            count_successful_builds: int = 0
            count_failed_builds: int = 0
            count_waiting_builds: int = 0
            for build in response_builds.json():
                match build['status']:
                    case 'SUCCESSFUL':
                        count_successful_builds += 1
                    case 'FAILED':
                        count_failed_builds += 1
                    case 'WAITING':
                        count_waiting_builds += 1
                    case _:
                        pass
            metric_s.add_sample('onedev_successful_builds', value=count_successful_builds, labels={})
            yield metric_s

            metric_f.add_sample('onedev_failed_builds', value=count_failed_builds, labels={})
            yield metric_f

            metric_w.add_sample('onedev_waiting_builds', value=count_waiting_builds, labels={})
            yield metric_w

        except requests.exceptions.RequestException as err:
            print(f'\nSomething happened:\n\n{err}\n')


if __name__ == '__main__':  # pragma: no cover
    start_http_server(8000)
    REGISTRY.register(BuildsCollector())
    while True:
        pass
