[![pipeline status](https://gitlab.com/mbhd/prometheus-exporter/badges/main/pipeline.svg)](https://gitlab.com/mbhd/prometheus-exporter/-/commits/main)  [![coverage report](https://gitlab.com/mbhd/prometheus-exporter/badges/main/coverage.svg)](https://gitlab.com/mbhd/prometheus-exporter/-/commits/main)  

# Group Project MBHD

- Anton Arntsen 
- Jonna Fleischander
- Kjell Bovin
- Markus Sköld
- Sem Haile 
- Tomas Karlsson 
- Viktor Berg 

## Prerequisites:

- Docker & Docker-compose
- Cloned copy of the repository

## So what are we doing?
This document will provide guidance for you to set up and reach a stack of five different services:
- **OneDev** for version control and CI/CD
- **Selfhosted Docker Registry** used for delivery
- **Prometheus**  for monitoring
- **Grafana** for visualizing metrics
- **Metric exporter** for exporting metrics from OneDev API to Prometheus 
___

# Adjusting files
1. Open prometheus/prometheus.yml. In row 24 change __only__ `localhost` to your computers IP address. Be sure to keep `:8000` directly after your IP address.
   - How to find [local IP address](https://www.howtouselinux.com/post/check-ip-address-in-linux)
2. Commit changes:
   - `git add prometheus/prometheus.yml`
   - `git commit -m 'changed ip address to my personal ip'`

# Set up services
1. Run `docker-compose up` from project root directory  
When everything is up and running you will have an output that will look something like this:  
![Docker-compose output](docs/fig1.png)
2. Open a terminal window. (Make sure to navigate to the cloned repository)
3. To see if all services is up and running run `docker ps`

## How to fix docker
If the following error message appears: Got permission denied while trying to connect to the Docker daemon socket.

    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker

## Set up OneDev  
Point your favourite web browser to [http://localhost:6610](http://localhost:6610)  
1. You will be prompted to create an administrator account. Choose a _login name, password, full name_ and an _email address_.  
2. Specify server URL. It will probably already be filled in. In case not, enter `http://localhost:6610` where asked to.
3. Click __Finish__


## Create a project in OneDev  
1. From OneDev projects page, add a project by clicking the plus sign to your right.  
2. Name your project `funny_project` and click on __Create__  
3. Copy and paste the following git-commands in your terminal window:  
   - `git remote add onedev http://localhost:6610/funny_project`  
   - `git push -u onedev main`  
     You may get an error when pushing to main if your default branch is configured as master. The fast and easy solution is to push to _master_ instead of _main_  
4. Your project should now contain the same files as found in this repo.


## Create Job Executor  
1. Select __Administration__ --> __Job Executors__ in the menu on your left.  
2. Click on __Add Executor__.  ![Add Executor](docs/fig2.png)  
3. Choose __Server Docker Executor__ in the drop down menu ![Server Docker Executor](docs/fig3.png)  
4. Name the job executor _server-docker-executor_ 


## Test Job Executor
1. Click on **Test** on the bottom of the page.  
2. Specify _Docker image_ **alpine:latest**  
3. Click _Ok_ to perform test.
4. If job executor tested successfully, click _Ok_ and _Save_  


## ADD PROJECT SECRETS
1. Navigate to `funny_project` page and go to 'settings' at the bottom of the left menu
2. Select 'Build' and 'Job Secrets'
3. Click on the '+' sign to add a new secret: ![Add Secret](docs/add_secret.png)
4. Add a secret: in the 'Name' section, insert `ONEDEV_USER` and into 'Value' section, insert your onedev username
5. Add a secret: in the 'Name' section, insert `ONEDEV_PASSWORD` and into 'Value' section, insert your onedev password
6. Add a secret: in the 'Name' section, insert `IP_ADDRESS` and into 'Value' section, insert your IP address for you local machine (i.e the same one you used to set up prometheus.yml)


## Pipeline  
Pipeline is already created and it should be in your project.  


## EXPORT ENVIRONMENT VARIABLES  
From your terminal window, you need to add the environment variables to run the exporter. Copy these commands into your terminal:  
(Replace \<...\> with your actual username, password or ip address)  
   `export ONEDEV_USER=`\<onedev username\>  
   `export ONEDEV_PASSWORD=`\<onedev password\>  
   `export IP_ADDRESS=`\<local ip address\>  


# Start the exporter
In the project root directory:

   Run the image in a container:  
      `docker run -e ONEDEV_USER=$ONEDEV_USER -e ONEDEV_PASSWORD=$ONEDEV_PASSWORD -e IP_ADDRESS=$IP_ADDRESS --publish 8000:8000 localhost:5000/mylittleimage`   

## The different services can now be reached via the following addresses:
 OneDev: http://localhost:6610/  
 Prometheus: http://localhost:9090/  
 Grafana: http://localhost:3000/  
 Docker registry: http://localhost:5050/



## Check Prometheus
Visit: http://localhost:9090 to reach prometheus 

In the menu at the top, Click Status and select Targets.

### IF everything works:

The __Onedev__ target should have it's _State_ as UP.

### IF NOT:

The problem in this case is most likely the target IP address. Check if the 'Endpoint' points to the same IP address as your current computer. If not, navigate to the prometheus directory inside the cloned gitlab repo and confirm that the target IP for the job 'Onedev' is the correct one. 

If you have followed this documentation correctly, this should not be a problem.

## Create Dashboards in Grafana
Grafana provides a range of tutorials on the first page, but the one that's of interest for us is to create a new dashboard to customize according to our needs

1. Start of by visiting http://localhost:3000/ 
2. Default username and password are: __admin__, __admin__ (Grafana will ask you to provide a stronger username and password, but that choice is up to you, See official documentation section (Sing in to Grafana): https://grafana.com/docs/grafana/v9.0/getting-started/build-first-dashboard/)  
3. At the start page click on the '+' sign in the left menu panel, then click on __Add an empty panel__  
4. In the __Query__ tab under the panel click on the tab that says: __-- Grafana --__ and change that to __Prometheus__ ![Grafana/Prometheus](docs/grafana1.png)
5. In the __Metrics__ tab, you will get different metric types to choose from. Here you can find your custom metrics. ![Getting Metrics](docs/grafana2.png)
6. Choose the __onedev__ category, and select __onedev_projects__ metric from the menu
7. In the tab to the right of the screen you can find a __Settings__ and a __Visualization__ tab like so: ![Settings and Visualization](docs/grafana3.png) 
   - In the __Settings__ tab you can change the name and description of your panel
   - In the __Visualization__ tab you can choose how you want to visualize the data
8. Click save in the top right corner of the page, Enter a fitting name for your dashboard and click __Save__ once more
   - BONUS: If you want to add more panels to your dashboard, click on the marked out button while in your dashboard page: ![Add New Panel](docs/grafana4.png)
   - List of custom metrics and types under the __onedev__ category as of now:
      - onedev_open_pull_requests (Gauge)
      - onedev_merged_pull_requests (Counter)
      - onedev_discarded_pull_requests (Counter)
      - onedev_successful_builds_total (Counter)
      - onedev_failed_builds_total (Counter)
      - onedev_waiting_builds (Gauge)
      - onedev_projects (Gauge)
      - onedev_number_of_users (Gauge)

With these steps you should have sufficient knowledge to create and manage your Grafana dashboards.

___

# Using flow branching strategy
- dev
- feat_docs
- test_users
- Anton
- hot-fix
- bug onedev
- feat_requiremnets
- main
- sem

# References
- [Control startup and shutdown order in docker-compose](https://docs.docker.com/compose/startup-order/)
- [How to use generators and yield in Python](https://realpython.com/introduction-to-python-generators/)
- [How to unit test Prometheus instrumentation](https://www.robustperception.io/how-to-unit-test-prometheus-instrumentation/)
- [Prometheus metric and label naming](https://prometheus.io/docs/practices/naming/)
- [Transforming remote JSON into Prometheus metrics](https://levelup.gitconnected.com/transforming-remote-json-into-prometheus-metrics-334d772df38a)
- [Grafana Getting Started](https://grafana.com/docs/grafana/v9.0/getting-started/build-first-dashboard/)
